NPCharacterResponse = new Mongo.Collection('n_p_character_response');

NPCharacterResponse.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  damageNum:{
      type:Number
  },
  objectCode:{
    type:String
  },
  scenarioCodes : {
    type:[String],
    optional: true
  },
  playerRequestCodes : {
    type:[String],
    optional: true
  }

}));
/*
 * Add query methods like this:
 *  NPCharacterResponse.findPublic = function () {
 *    return NPCharacterResponse.find({is_public: true});
 *  }
 */